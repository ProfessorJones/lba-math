#!/usr/bin/python3

def do_lba(X, HPC, SPT, Y, C, H, S):
    LBA = (C * HPC + H) * SPT + (S-1)
    print("For geometry %s %s %s of a disk with %s sectors CHS %s %s %s is LBA = %s" % (X, HPC, SPT, Y, C, H, S, LBA))

def main():
    do_lba(1020, 16, 63, 1028160, 4, 3, 2)
    do_lba(1008, 4, 255, 1028160, 4, 3, 2)
    do_lba(64, 255,  63, 1028160, 3, 2, 4)
    do_lba(2142, 15, 32, 1028160, 5, 1, 5)

if __name__ == "__main__":
    main()
